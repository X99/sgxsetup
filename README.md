This ansible playbook sets up Intel SGX on a computer.

To be more precise, it:

- installs dependencies
- installs Docker/docker-compose
- installs Intel SGX SDK and driver's latest stable version
- installs Gramine (if explicitely required, see below) with the appropriate kernel version.

# Running on a Vagrant test VM

This playbook is shipped with a vagrantfile you can use for tests purposes.

To do so:

```
vagrant up
```
Ths VagrantFile is made so that the first VM will use 192.168.3.10, the second 192.168.3.11 and so on.
Then, SSH onto the VM to accept the key and launch the playbook using

```
ansible-playbook sgx_setup.yml -i '192.168.3.10,192.168.3.11,' -u vagrant
```

# Running on a real machine

SSH onto the machine at least once, then

```
ansible-playbook sgx_setup.yml -i 'YOUR_IPS,' -u YOUR_USER -K -e "install_gramine=true"
```

The command will ask for sudo ("become") password.

# Troubleshooting

If the `make preparation` stage seems stuck, it might be because of `wget` trying to use IPV6. If so, SSH to the machine and disable IPV6 like so:

```
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.lo.disable_ipv6=1
```
